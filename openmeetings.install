<?php

/**
 * @file
 * Install, update and uninstall functions for the openmeetings module.
 */

/**
 * Implements hook_install().
 */
function openmeetings_install() {
  /** @var \Drupal\user\RoleInterface $role */
  $role = \Drupal\user\Entity\Role::create(array(
    'id' => 'openmeetings_moderator',
    'label' => 'Openmeetings Moderator',
  ));
  $role->save();
}

/**
 * Implements hook_uninstall().
 */
function openmeetings_uninstall() {
  // Delete a user role by name/ID.
  entity_delete('user_role', 'openmeetings_moderator');
}

/**
 * Implements hook_schema().
 */
function d8_openmeetings_schema() {
  $schema = array();
  $schema['openmeetings_conf_users'] = array(
    'description' => 'Openmeetings Conference Users list.',
    'fields' => array(
      'aid' => array(
        'type' => 'serial',
        'description' => 'Primary Key indicating ID for conference user',
        'not null' => TRUE,
      ),
      'uid' => array(
        'type' => 'int',
        'description' => 'Drupal User ID',
        'not null' => FALSE,
      ),
      'cid' => array(
        'type' => 'int',
        'description' => 'Local Openmeetings conference ID',
        'not null' => TRUE,
      ),
      'email' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'email_status' => array(
        'type' => 'int',
        'default' => 0,
        'description' => 'Status of the email send to user',
      ),
    ),
    'primary key' => array('aid'),
  );
  $schema['openmeetings_conference'] = array(
    'description' => 'Openmeetings Conferences',
    'fields' => array(
      'cid' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'description' => 'Openmeetings local conference ID',
      ),
      'om_cid' => array(
        'type' => 'int',
        'description' => 'Openmeetings Remote conference ID',
      ),
      'title' => array(
        'type' => 'varchar',
        'size' => 'normal',
        'length' => 255,
        'not null' => TRUE,
      ),
      'description' => array(
        'type' => 'text',
      ),
      'type' => array(
        'type' => 'int',
        'not null' => TRUE,
        'description' => 'Conference Type',
      ),
      'conference_member_limit' => array(
        'type' => 'int',
        'not null' => TRUE,
        'description' => 'Conference Number Limit',
      ),
      'start_time' => array(
        'type' => 'int',
        'description' => 'Conference start time',
      ),
      'end_time' => array(
        'type' => 'int',
        'description' => 'Conference end time',
      ),
      'notify_user' => array(
        'type' => 'int',
        'not null' => TRUE,
        'description' => 'Notify User',
        'default' => 0,
      ),
      'is_moderated' => array(
        'type' => 'int',
        'not null' => TRUE,
        'description' => 'Is Conference Moderated',
        'default' => 0,
      ),
      'is_audio_only' => array(
        'type' => 'int',
        'not null' => TRUE,
        'description' => 'Is conference audio only',
        'default' => 0,
      ),
      'allow_user_questions' => array(
        'type' => 'int',
        'not null' => TRUE,
        'description' => 'Allow user questions',
        'default' => 0,
      ),
      'allow_recording' => array(
        'type' => 'int',
        'not null' => TRUE,
        'description' => 'Allow recording',
        'default' => 0,
      ),
      'areas_of_interests_tid' => array(
        'type' => 'varchar',
        'length' => 1000,
      ),
      'status' => array(
        'type' => 'int',
        'not null' => TRUE,
        'description' => 'Conference Status',
        'default' => 0,
      ),
      'created_by' => array(
        'type' => 'int',
        'not null' => TRUE,
        'description' => 'Conferences Created By',
        'default' => 1,
      ),
      'created_on' => array(
        'type' => 'int',
        'description' => 'Conference Created On',
      ),
    ),
    'primary key' => array('cid'),
  );
  return $schema;
}
